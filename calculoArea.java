package jogos.lpa;
import java.util.Scanner;
public class calculoArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int operacao; 
		double l,b,h,r,res;
		final double pi = 3.14;
		
		System.out.println("Qual área deseja calcular:"+"\nCalcular área Quadrado (1)" + "\nCalcular área Retângulo (2)" + "\nCalcular área Triângulo (3)" + "\nCalcular área Círculo (4)");
		operacao = input.nextInt();
		switch (operacao) {
		case 1:
			System.out.println("Informe o valor do lado(l):");
			l = input.nextInt();
			res = l * l;
			System.out.println("Área do quadrado: " + res);
			break;
		case 2:
			System.out.println("Informe o valor de base(b):");
			b = input.nextInt(); 
			System.out.println("Informe o valor da altura(h):");
			h = input.nextInt();
			res = b * h;
			System.out.println("Área do retângulo: " + res);
			break; 
		case 3:
			System.out.println("Informe o valor de base(b):");
			b = input.nextInt(); 
			System.out.println("Informe o valor da altura(h):");
			h = input.nextInt();
			res = (b * h) / 2;
			System.out.println("Área do triângulo: " + res);
			break; 
		case 4:
			System.out.println("Informe o valor do raio(r):");
			r = input.nextInt(); 
			System.out.println("Informe o valor da altura(h):");
			h = input.nextInt();
			res = 2*pi*r;
			System.out.println("Área do círculo: " + res);
			break;
			
		default:
			break;
		}
	}

}
